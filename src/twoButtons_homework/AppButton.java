package twoButtons_homework;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.util.ArrayList;
import java.util.List;

import javax.swing.JButton;

public class AppButton extends JButton implements Runnable {

	private Boolean pushed = false;
	private static List<Boolean> listOfPressedInstances = new ArrayList<Boolean>();
	private Thread t1;
	private volatile boolean threadRunning = true;
	private static int countInstances = 0;
	private int instance;

	public void initiate() {
		threadRunning = true;
	}

	public void terminate() {
		threadRunning = false;
	}

	@Override
	public void run() {
		while (threadRunning) {
			System.out.println("thread running...");
			alternate(t1);
			behaviorOnClick(t1);// here's the little bug , is systematic after it gets out of alternate it gets
			// here,it seems i've got an concurrent error
		}
	}

	public int getInstances() {
		return countInstances;
	}

	public void removeInstance() {
		countInstances -= 1;
	}

	public List<Boolean> getListOfPressedInstances() {
		return listOfPressedInstances;
	}

	public void removeFromListOfPressedInstances(int index) {
		terminate();// setting the flag to false to stop the thread ,in this way i'm solving the
					// concurrent thread error after deleting an instance and also the thread will
					// eat less resources in case it occurs to be zombie thread
		System.out.println("...thread safely stoped");
		listOfPressedInstances.remove(index);
	}

	public AppButton() {
		countInstances += 1;
		instance = countInstances;
		System.out.printf("Instance : %s\n", countInstances);
		listOfPressedInstances.add(this.pushed);
		this.setPreferredSize(new Dimension(100, 50));
		this.setFont(new Font("Arial", Font.PLAIN, 20));
		this.addActionListener(e -> buttonAction());
	}

	private void behaviorOnClick(Thread t) {
		while (!areAllTrue(listOfPressedInstances) && this.pushed) {
			try {
				t.sleep(200);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
			if (this.getBackground() != Color.GREEN) {
				this.setBackground(Color.GREEN);
			} else if (this.getBackground() == Color.GREEN) {

				this.setBackground(Color.RED);
			}

		}
		if (!pushed) {
			this.setBackground(null);
		}

	}

	private void buttonAction() {
		if (!pushed) {
			initiate();
			t1 = new Thread(this);
			t1.start();
			pushed = true;
			if (listOfPressedInstances.get(instance - 1) == false) {
				listOfPressedInstances.set(instance - 1, pushed);
			}
		} else {
			pushed = false;
			if (listOfPressedInstances.get(instance - 1) == true) {
				listOfPressedInstances.set(instance - 1, pushed);
			}
			terminate();
			System.out.printf("threadRunning = %s\n", threadRunning);

		}
		System.out.println(listOfPressedInstances);
	}

	public static boolean areAllTrue(List<Boolean> list) {
		for (Boolean b : list)
			if (!b)
				return false;
		return true;
	}

	private void alternate(Thread t) {// i can merge this with behaviorOnClick()
		while (areAllTrue(listOfPressedInstances)) {
			try {
				t.sleep(200);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}

			if (this.getBackground() != Color.YELLOW) {

				this.setBackground(Color.YELLOW);

			} else if (this.getBackground() == Color.YELLOW) {

				this.setBackground(Color.BLUE);

			}
		}
		if (!pushed) {
			this.setBackground(null);
		}
	}

}