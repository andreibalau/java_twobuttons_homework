package twoButtons_homework;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.util.ArrayList;
import java.util.List;

import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.SpringLayout;
import javax.swing.SwingUtilities;

public class MainFrame {

	private JFrame frame;
	private JPanel mainFramePanel;
	private SpringLayout layout;
	private AppButton myButton;
	private JButton addButton;
	private JButton dropButton;
	private List<AppButton> buttonsList;
	private static AppButton prevButtonInserted;

	public MainFrame() {
		buttonsList = new ArrayList<AppButton>();
		frame = new JFrame("Two Buttons Homework");
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

		mainFramePanel = new JPanel();
		layout = new SpringLayout();

		mainFramePanel.setBorder(BorderFactory.createLineBorder(Color.black));
		mainFramePanel.setLayout(layout);

		addButton = new JButton("Add");
		addButton.setPreferredSize(new Dimension(100, 50));
		addButton.setFont(new Font("Arial", Font.PLAIN, 20));

		dropButton = new JButton("Drop");
		dropButton.setPreferredSize(new Dimension(100, 50));
		dropButton.setFont(new Font("Arial", Font.PLAIN, 20));

		addButton.addActionListener(e -> addAction());
		dropButton.addActionListener(e -> dropAction());

		mainFramePanel.add(addButton);
		mainFramePanel.add(dropButton);

		layout.putConstraint(SpringLayout.VERTICAL_CENTER, addButton, 100, SpringLayout.VERTICAL_CENTER,
				mainFramePanel);
		layout.putConstraint(SpringLayout.VERTICAL_CENTER, dropButton, 100, SpringLayout.VERTICAL_CENTER,
				mainFramePanel);
		layout.putConstraint(SpringLayout.HORIZONTAL_CENTER, addButton, -60, SpringLayout.HORIZONTAL_CENTER,
				mainFramePanel);
		layout.putConstraint(SpringLayout.WEST, dropButton, 20, SpringLayout.EAST, addButton);

		mainFramePanel.setLayout(layout);

		frame.add(mainFramePanel, BorderLayout.CENTER);
		frame.setSize(600, 500);
		frame.setVisible(true);
		frame.setLocationRelativeTo(null);

	}

	private void addAction() {

		myButton = new AppButton();
		mainFramePanel.add(myButton);
		buttonsList.add(myButton);

		// setting postion of inserted buttons in mainFramePanel
		if (buttonsList.size() - 1 == 0) {
			layout.putConstraint(SpringLayout.VERTICAL_CENTER, myButton, 0, SpringLayout.VERTICAL_CENTER,
					mainFramePanel);
			layout.putConstraint(SpringLayout.HORIZONTAL_CENTER, myButton, -100, SpringLayout.HORIZONTAL_CENTER,
					mainFramePanel);
			prevButtonInserted = myButton;
		} else if (buttonsList.size() - 1 != 0) {
			AppButton lastButtonIn = buttonsList.get(buttonsList.size() - 1);
			layout.putConstraint(SpringLayout.VERTICAL_CENTER, lastButtonIn, 0, SpringLayout.VERTICAL_CENTER,
					mainFramePanel);
			layout.putConstraint(SpringLayout.WEST, lastButtonIn, 20, SpringLayout.EAST, prevButtonInserted);
			prevButtonInserted = lastButtonIn;
		}
		SwingUtilities.updateComponentTreeUI(frame);
	}

	private void dropAction() {
		if (buttonsList.size() != 0) {
			mainFramePanel.remove(buttonsList.get(buttonsList.size() - 1));// taking out the button from mainFramePanel
			buttonsList.remove(buttonsList.get(buttonsList.size() - 1));// also taking it out from the list of buttons
																		// added
			if (buttonsList.size() != 0) {
				/*
				 * setting the previous button inserted the last button from buttonsList because
				 * in case i want to add new button after removing one to keep the nice layout
				 */
				prevButtonInserted = buttonsList.get(buttonsList.size() - 1);
			}
			List<Boolean> ListOfPressedInstances = myButton.getListOfPressedInstances();
			myButton.removeFromListOfPressedInstances((ListOfPressedInstances.size() - 1));
			System.out.printf("Instance removed : %s\n", myButton.getInstances());
			myButton.removeInstance();
			System.out.println(ListOfPressedInstances);
			SwingUtilities.updateComponentTreeUI(frame);
		}
	}
}
